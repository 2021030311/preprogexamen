//PROYECTO TERMINADO
package Controlador;
import Modelo.Cliente;
import Modelo.CuentaBancaria;
import Vista.dlgCuenta;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JFrame;

public class Controlador implements ActionListener{
    private CuentaBancaria cuenta;
    private dlgCuenta vista;

    public Controlador(CuentaBancaria cuenta, dlgCuenta vista){
        this.cuenta = cuenta;
        this.vista = vista;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnDeposito.addActionListener(this);
        vista.btnRetiro.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnSalir.addActionListener(this);
    }
    private void iniciarVista(){
        vista.setTitle(" == CUENTA BANCARIA == ");
        vista.setSize(665, 460);
        vista.setVisible(true);
    }
    
    public void limpiar() {
        vista.bFemenino.setSelected(false);
        vista.bMasculino.setSelected(false);
        vista.txtNumCuenta.setText("");
        vista.txtNombre.setText("");
        vista.txtDomicilio.setText("");
        vista.txtFechaNac.setText("");
        vista.txtBancoNombre.setText("");
        vista.txtFechaAper.setText("");
        vista.txtPorcentaje.setText("");
        vista.txtSaldo.setText("");
        vista.txtCantidad.setText("");
        vista.jblNewSaldo.setText("");
    }

   
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btnNuevo){
            vista.txtNombre.setEnabled(true);
            vista.txtFechaNac.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.bFemenino.setEnabled(true);
            vista.bMasculino.setEnabled(true);
            vista.txtBancoNombre.setEnabled(true);
            vista.txtFechaAper.setEnabled(true);
            vista.txtPorcentaje.setEnabled(true);
            vista.txtSaldo.setEnabled(true);
            vista.txtNumCuenta.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
        }
        
        if(e.getSource() == vista.btnGuardar){
            String opc = "";
            
            try{
                cuenta.setFecha(vista.txtFechaAper.getText());
                cuenta.setNombreBanco(vista.txtBancoNombre.getText());

                cuenta.setCli(new Cliente(
                        vista.txtNombre.getText(),
                        vista.txtDomicilio.getText(),
                        vista.txtFechaNac.getText(),
                        opc
                    )
                );
                if(vista.bMasculino.isSelected() == true){
                    opc = "Masculino";
                }
                else if(vista.bFemenino.isSelected() == true){
                        opc = "Femenino";
                }
                else{
                    throw new IllegalArgumentException("Selecciona un sexo primero");
                }
                
                if(Float.parseFloat(vista.txtNumCuenta.getText()) <= 0){
                    throw new IllegalArgumentException("Ingrese un numero valido");
                }
                
                if(Float.parseFloat(vista.txtSaldo.getText()) < 0){
                    throw new IllegalArgumentException("El saldo no puede ser negativo");
                }
                
                if(Float.parseFloat(vista.txtPorcentaje.getText()) < 0){
                    throw new IllegalArgumentException("El porcentaje no puede ser negativo");
                }
                
                cuenta.setNumCuenta(Integer.parseInt(vista.txtNumCuenta.getText()));
                cuenta.setPorcenRendimiento(Float.parseFloat(vista.txtPorcentaje.getText()));
                cuenta.setSaldo(Float.parseFloat(vista.txtSaldo.getText()));
                limpiar();
                
                vista.txtCantidad.setEnabled(true);
                vista.btnDeposito.setEnabled(true);
                vista.btnRetiro.setEnabled(true);
                
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
            }catch(Exception ex2){
                JOptionPane.showMessageDialog(vista, "Error :" + ex2.getMessage());
            }
            
        }
        
        if(e.getSource() == vista.btnMostrar){
            vista.txtNumCuenta.setText(Integer.toString(cuenta.getNumCuenta()));
            vista.txtNombre.setText(cuenta.getCli().getNombreCliente());
            vista.txtDomicilio.setText(cuenta.getCli().getDomicilio());
            vista.txtFechaNac.setText(cuenta.getCli().getFechaNacimiento());
            
            if(cuenta.getCli().getSexo().equals("Masculino")){
                vista.bMasculino.setSelected(true);
            }
            else if(cuenta.getCli().getSexo().equals("Sexo Femenino")){
                vista.bFemenino.setSelected(true);
            }
            vista.txtBancoNombre.setText(cuenta.getNombreBanco());
            vista.txtFechaAper.setText(cuenta.getFecha());
            vista.txtPorcentaje.setText(Float.toString(cuenta.getPorcenRendimiento()));
            vista.txtSaldo.setText(Float.toString(cuenta.getSaldo()));
            vista.jblNewSaldo.setText(Float.toString(cuenta.getSaldo()));
        }
        
        if(e.getSource() == vista.btnRetiro){
             
            try{
                if(Float.parseFloat(vista.txtCantidad.getText()) <= 0){
                    throw new IllegalArgumentException("Ingrese una cantidad valida");
                }
                
                if(cuenta.retirar(Float.parseFloat(vista.txtCantidad.getText())) == true){
                    vista.jblNewSaldo.setText(Float.toString(cuenta.getSaldo()));
                    JOptionPane.showMessageDialog(vista, "Dinero retirado");
                    
                }   
                else{
                    throw new IllegalArgumentException("Dinero insuficiente");
                    
                }
            }catch(NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
            catch(IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
           // boolean con = cuenta.retirar(Float.parseFloat(vista.txtCantidad.getText()));
            vista.txtCantidad.setText("");
        }
        
        if(e.getSource() == vista.btnDeposito){
            try{
                if(Float.parseFloat(vista.txtCantidad.getText()) <= 0){
                    throw new IllegalArgumentException("Ingrese una cantidad validad");
                }
                else{
                    cuenta.depositar(Float.parseFloat(vista.txtCantidad.getText()));
                    vista.jblNewSaldo.setText(Float.toString(cuenta.getSaldo()));
                    JOptionPane.showMessageDialog(vista, "Dinero depositado");
                }
            }catch(NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                
            }
            catch(IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                
            }
            vista.txtCantidad.setText("");
        }
        
        if(e.getSource() == vista.btnLimpiar){
            limpiar();
        }
        
        if(e.getSource() == vista.btnSalir) {
            int option = JOptionPane.showConfirmDialog(vista, "¿Esta seguro que desea salir?", "Cerrar", JOptionPane.YES_NO_OPTION);
            if(option == JOptionPane.YES_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
    }
    
    public static void main(String[] args) {
        CuentaBancaria cuenta = new CuentaBancaria();
        dlgCuenta vista = new dlgCuenta(new JFrame(), true);
        Controlador contra = new Controlador(cuenta, vista);
        contra.iniciarVista();
    }
}

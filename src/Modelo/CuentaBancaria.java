
package Modelo;


public class CuentaBancaria {
    int numCuenta;
    String fecha, nombreBanco;
    float porcenRendimiento, saldo;
    Cliente cli;

    public CuentaBancaria(){
        numCuenta = 0;
        fecha = "";
        nombreBanco = "";
        porcenRendimiento = 0;
        saldo = 0;
        cli = new Cliente();
    }
    public CuentaBancaria(int numCuenta, String fecha, String nombreBanco, float porcenRendimiento, float saldo, Cliente cli) {
        this.numCuenta = numCuenta;
        this.fecha = fecha;
        this.nombreBanco = nombreBanco;
        this.porcenRendimiento = porcenRendimiento;
        this.saldo = saldo;
        this.cli = cli;
    }
    
    public CuentaBancaria(CuentaBancaria cuenta) {
        this.numCuenta = cuenta.numCuenta;
        this.fecha = cuenta.fecha;
        this.nombreBanco = cuenta.nombreBanco;
        this.porcenRendimiento = cuenta.porcenRendimiento;
        this.saldo = cuenta.saldo;
        this.cli = cuenta.cli;
    }

    public int getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public float getPorcenRendimiento() {
        return porcenRendimiento;
    }

    public void setPorcenRendimiento(float porcenRendimiento) {
        this.porcenRendimiento = porcenRendimiento;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public Cliente getCli() {
        return cli;
    }

    public void setCli(Cliente cli) {
        this.cli = cli;
    }
    
    public void depositar(float depo){
        this.saldo += depo;
    }
    
    public boolean retirar(float retiro){
        if (retiro > this.saldo){
            return false;
        }
        this.saldo = this.saldo - retiro;
        return true;
    }
    
    public float getCalcularCredits(){
        return (this.porcenRendimiento * this.saldo)/365;
    }
}
